import random
import socket

myPort = 1234
mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind(('', myPort))
mySocket.listen(5)

random.seed()

if __name__ == "__main__":
    try:
        url_list = ["https://www.google.com/", "https://www.python.org/", "https://www.amazon.es/",
                    "https://www.twitter.com/", "https://en.wikipedia.org/", "https://es.linkedin.com/",
                    "https://openai.com/", "https://www.instagram.com/", "https://www.youtube.com/"]
        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received:")
            print(recvSocket.recv(2048))

            # Resource name for next url
            nextPage = random.choice(url_list)
            # HTML body of the page to serve
            htmlBody = "<h1>Redirection</h1>" + '<p>Next page: <a href="' \
                + nextPage + '">' + nextPage + "</a><br>Si no ha cambiado prueba a actualizar la pagina</p>"
            recvSocket.send(b"HTTP/1.1 200 OK \r\n\r\n" +
                    b"<html><body>" + htmlBody.encode('ascii') + b"</body></html>" +
                    b"\r\n")
            recvSocket.close()

    except KeyboardInterrupt:
        print("Closing binded socket")
        mySocket.close()
